---
title: On Spiritual Warfare
author: remy
feature: spiritual-warfare.jpg
description: Human beings are a bunch of superstitious weirdos. And we get worse when you start adding in spirituality.
date: 2021-04-25
categories: ["Christian Life"]
tags: ["Spirituality", "Spiritual Life", "Christianity", "Paul"]
---

When I was growing up, my mother used to have this woman over to the house every Thursday night, along with other women of the church. This woman - Jan, was her name - was blind, claimed to have the entire bible memorized word for word, and taught "spiritual warfare".

My mom was quick to get sucked down this rabbit hole of false teaching and she, along with the other women of the 'Bible study' group, would spend time shouting and hollering at the devil and breaking "generational curses" and whatever else.

This was generally followed by refreshments and giving a tithe offering to Ms. Jan.

## There were many red flags.

To me as a child, I always found there to be a great many red flags with this woman and her teaching. For one, she really did take up an offering at every "Bible Study" she put on - and she put one just about every night at different homes.

I wasn't generally allowed in the entire downstairs of the house during the Bible Study meetings because they were talking about frightening things - demonic power and what not - that frightened me.

<indent>After all, doesn't that Bible Verse say that Christ gives us a peace that surpasses all understanding, "unless we're listening to someone do great battle with Satan"?</indent>

Jan also taught that, while you couldn't curse your enemies, you could trick God into destroying people by praying they "get what they deserve."

<indent>lol</indent>

Eventually, Jan was found ruled a false teacher by the lead pastor of our church and was literally excommunicated and told never to return.

She stopped hosting weekly shout-fests at my house and disappeared into obscurity.

My mother told me what happened, told me that Jan was a false teacher and that she wouldn't be coming back, but for some reason, my mom still continued to believe the things that Jan taught her.

To this day, I'm certain she still believes that she needs to pray "hedges of protection" around things, and pray for "warring angels to stand outside doorways", and the like.

## Where does all of this nonsense come from?

First and foremost, I believe the idea of "spiritual warfare" comes from a fundamental misunderstanding and misapplication of the Gospel.

But usually when you talk to people about "spiritual warfare", the offending scripture twisting that pops up the fastest is [Ephesians 6](http://biblehub.com/esv/ephesians/6.htm), specifically verses 10 through 17:

>10Finally, be strong in the Lord and in the strength of his might. 11Put on the whole armor of God, that you may be able to stand against the schemes of the devil. 12For we do not wrestle against flesh and blood, but against the rulers, against the authorities, against the cosmic powers over this present darkness, against the spiritual forces of evil in the heavenly places.
>
>13Therefore take up the whole armor of God, that you may be able to withstand in the evil day, and having done all, to stand firm. 14Stand, therefore, having fastened on the belt of truth, and having put on the breastplate of righteousness, 15and, as shoes for your feet, having put on the readiness given by the gospel of peace. 16In all circumstances take up the shield of faith, with which you can extinguish all the flaming darts of the evil one; 17and take the helmet of salvation, and the sword of the Spirit, which is the word of God,

So this is where people get most of their mumbo jumbo on spiritual warfare. People like to interpret the Bible themselves, often forgetting that the only real interpretation of the Bible is the Bible itself.

You'll notice up there a little comma. I bring that up because the thought doesn't end at verse 17, but rather continues on to [verse 18](http://biblehub.com/esv/ephesians/6-18.htm):

>praying at all times in the Spirit, with all prayer and supplication.

Now what's neat about this verse is that there is actually a word in the Greek that, like, no one bothers to translate, but is actually a pretty huge deal. That word is <mark>διὰ</mark>. In this case, it's a *[genitive causal preposition](https://en.wiktionary.org/wiki/%CE%B4%CE%B9%CE%AC)* taking the meaning of, "by" or "through".

You can see the word when you look at the passage in Greek:

>καὶ τὴν περικεφαλαίαν τοῦ σωτηρίου δέξασθε, καὶ τὴν μάχαιραν τοῦ Πνεύματος, ὅ ἐστιν ῥῆμα Θεοῦ, <mark>διὰ</mark> πάσης προσευχῆς καὶ δεήσεως, προσευχόμενοι ἐν παντὶ καιρῷ ἐν Πνεύματι, καὶ εἰς αὐτὸ ἀγρυπνοῦντες ἐν πάσῃ προσκαρτερήσει καὶ δεήσει περὶ πάντων τῶν ἁγίων,

Taking that little (but important) word into account, we can see that we are *take the full armor of God **by** praying in the Spirit*.

Now, how you interpret *praying in the Spirit* might be different from how I interpret it, but we can all agree it most assuredly does not mean shouting, "I PLACE THE HELMET OF SALVATION UPON MY HEAD!" loudly in my living room at 8:30 PM on a Thursday night.

You see, your true protection in "spiritual warfare", against those flaming darts of the enemy the Bible mentions, is prayer and scripture.

Don't forget that the thing [Paul says here](http://biblehub.com/esv/romans/10-17.htm) that protects you is the *shield of faith*, and we know that *faith comes by hearing, and hearing by the Word of God*.

And you are to put on this armor of God by *praying in the Spirit*.

### Paul is being metaphorical.
Don't forget that [Paul himself was a Pharisee](http://biblehub.com/esv/acts/23-6.htm) and was a very devout Jew. He knew the scripture backward and forwards and was using the "armor of God" as a metaphor here to Salvation, and once again revealing Christ in the Old Testament.

A quick reading of [Isaiah 59:15-17](http://biblehub.com/esv/isaiah/59.htm):

>The Lord saw it, and it displeased him
>that there was no justice.
><sup>16</sup>He saw that there was no man,
>and wondered that there was no one to intercede;
>then his own arm brought him salvation,
>and his righteousness upheld him.
><sup>17</sup>**He put on righteousness as a breastplate,
>and a helmet of salvation on his head;**
>he put on garments of vengeance for clothing,
>and wrapped himself in zeal as a cloak.

This passage in Isaiah is talking about Christ. It talks about how God looked down at humanity and found that there was no one to save us, so he decided to come down and do it himself.

<indent>If you want something done right...</indent>

Paul is saying that to withstand the enemy, you need to place yourself and your faith *in* Christ.

He isn't saying you need to go to some physically high place and combat the devil in prayer, or any such nonsense.

## What about 2nd Corinthians where Paul talks about our Divine Weapons and warfare?

Another oft-twisted scripture is 2nd Corinthians 10:3-4, where Paul talks about our weapons and our war not being against the flesh.

While there is a lot of truth that is drawn from the Bible by looking at inverse meanings, this is not one of those cases.

People claim that because Paul said we aren't warring against the flesh, it must mean that we're warring against ***SATAN*** AND EEEVVIIILLL.

Nah, bruh.

In fact, if you read just [one more verse](http://biblehub.com/esv/2_corinthians/10.htm), you'll see that Paul is quick to explain himself (as he always is):

><sup>3</sup>For though we walk in the flesh, we are not waging war according to the flesh. <sup>4</sup>For the weapons of our warfare are not of the flesh but have divine power to destroy strongholds.

Tell me, Mr. Paul, what are these strongholds?

><sup>5</sup>We destroy arguments and every lofty opinion raised against the knowledge of God, and take every thought captive to obey Christ,

It would seem that Paul isn't speaking about doing war against a great evil Satan or otherworldly force. In fact, it seems he's saying that our war isn't against men, but rather it's against their *ideas*.

It would seem the strongholds he spoke of here are actually just false beliefs, false doctrines (like "spiritual warfare"), and wrong thinking.

### Okay, fine, but Jesus himself said we should bind and loose principalities and the evil one!

Beloved, please open your spirit to this truth when I tell you: It's the devil that binds things. He is the one that creates bondage of every sort and imprisons men and women.

Christ is He who sets us free!

<indent>And we are free, indeed!</indent>

When Jesus said, "whatever you bind on earth will be bound in Heaven, and whatever you loose on earth will be loosed in Heaven," He wasn't talking about binding devils and casting them out.

He was talking about doctrines and teachings. He was giving us the authority to lead the church.

Binding and Loosing is a [Jewish legal term](https://en.wikipedia.org/wiki/Binding_and_loosing) that means *to forbid by an indisputable authority* and *to permit by an indisputable authority*, respectively.

# So how are we supposed to fight the devil, then?
So if Spiritual warfare is a bunch of bunk nonsense, then how are we to fight the devil?

The answer is simple, and may even shock you: **We aren't.**

Beloved, you don't need to *fight* the devil, because Christ *already fought him* - and Jesus won!

That is the core of the Gospel! The core of Christianity, and love, and grace! Christ has won. He is risen. He is Lord!

He has overcome every sin, every temptation, and even death itself, and now gives freely eternal life to all those who believe.

If you confess with your mouth that Jesus is Lord, and believe in your Heart that God raised him from the dead, then you will be saved!

In accepting Christ and recognizing his sacrifice for you, are you telling God that your *sin offering* was Christ at Calvary!

In this way, your sins transfer to Christ, and in return, his Righteousness transfers to you.

### So how did Jesus fight the devil?
When you tell people that they no longer need to fight the devil because Jesus already fought the devil (and won), they start to ask how Jesus fought the devil.

You know, if Jesus did it, then we should do it too - right?

Well if we look at the *Temptation of Christ* in [Matthew](http://biblehub.com/esv/matthew/4.htm), we see how Jesus fought the devil:

>1Then Jesus was led up by the Spirit into the wilderness to be tempted by the devil. 2And after fasting forty days and forty nights, he was hungry. 3And the tempter came and said to him, "If you are the Son of God, command these stones to become loaves of bread." 4But he answered,
>"It is written,
>"'Man shall not live by bread alone,
>but by every word that comes from the mouth of God.'"
>
>5Then the devil took him to the holy city and set him on the pinnacle of the temple 6and said to him, "If you are the Son of God, throw yourself down, for it is written,
>
>"'He will command his angels concerning you,'
>and
>"'On their hands, they will bear you up,
>lest you strike your foot against a stone.'"
>
>7Jesus said to him, "Again it is written, 'You shall not put the Lord your God to the test.'" 8Again, the devil took him to a very high mountain and showed him all the kingdoms of the world and their glory.
>9And he said to him, "All these I will give you if you will fall down and worship me." 10Then Jesus said to him, "Begone, Satan! For it is written,
>"'You shall worship the Lord your God
>and him only shall you serve.'"
>
><sup>1</sup><sup>1</sup>Then the devil left him, and behold, angels came and were ministering to him.

So let's take a look at how Jesus fought the devil, going from back to front:

#### He worshipped God
When you are in the throes of temptation or sin (yes, even if you're in the middle of sin), worship God. He is mighty and has the power to deliver you even at the moment.

#### He didn't test God (himself)
When the devil tempted him to throw himself off a cliff, Jesus responded by saying that you don't *test* God, you ***trust*** Him.

Not only should you trust God in the midst of your sin and crisis, you should avoid putting yourself in a situation where you are tempted and tested.

Jesus didn't throw himself off the cliff. He probably *would* have been fine, but he opted to avoid the temptation to sin altogether. You should probably follow in those steps.

#### He meditated on the Word of God
When Christ was hungry, the devil said, "I'm pretty sure you could literally materialize some food." Jesus responded by saying that man doesn't live by bread alone, but by the Word of God - the Bread of Life.

Now, this is super interesting because in this we see everything that we should do when confronted with the devil.

The total act of "spiritual warfare" for the believer consists of worshipping God, avoiding situations where you might be tempted, and feeding on *His word* if you do find yourself in a bad situation.

God wants you to sit down, be at rest, and feed on Him when you're tempted. When you're under assault by the devil, God wants you to trust Him and to focus on His word.

Not your surroundings, not the devil beating down the door, not your sin or your fear, but focus solely on His love for you.

To wit:

><sup>2</sup>He makes me lie down in green pastures.
>He leads me beside still waters.
>\[...\]
><sup>5</sup>You **prepare a table before me
>in the presence of my enemies;**
>you anoint my head with oil;
>my cup overflows.
>
>*~ [Psalm 23](http://biblehub.com/esv/psalms/23.htm)*

Beloved, God wants you to not put yourself in harm's way. He wants you to Worship him. And He wants you to feed on His Word, His Grace, His Presence, and His Spirit.

You don't need to do divine battle with the devil - I'm pretty sure Jesus has that handled. You don't need to bind and loose devils and cast them into darkness - Jesus will do all that.

Your job is to simply *rest in Him*.

For the Christian, Spiritual Warfare is trusting that Christ has already won the battle.

God bless you.