---
title: Oh no! What happened?
avoid: true
url: /donation/cancel
---

I'm not sure why you decided not to donate, but it's okay. While we enjoy support from our readers, we know that sometimes it just isn't possible to give what you want. Know that this project will always be funded by its founders and will be here in the future, should you be able to give later.
