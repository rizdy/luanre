---
title: "The Book of Concord"
---

This is a free copy of the 1580 Lutheran Confessional Documents via Project Wittenberg. It is currently a work in progress.

Unless otherwise noted, this text was translated in 1994 for [Project 
Wittenberg](http://www.projectwittenberg.org/) by Robert E. Smith and has been placed in the public domain 
by him. You may freely distribute, copy or print this text.  Please 
direct any comments or suggestions to Rev. Robert E. Smith of the Walther Library at:

E-mail: smithre@mail.ctsfw.edu

Address: 6600 N. Clinton St., Ft.  Wayne, IN 46825 USA

Phone: (260) 481-2123                        

Fax: (260) 481-2126

{{< section >}}