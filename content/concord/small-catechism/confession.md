---
title: "Confession"
feature: stand-in.png
---

# How One Should Teach the Uneducated to Confess

### What is confession?

Confession has two parts:

First, a person admits his sin

Second, a person receives absolution or forgiveness from the confessor, as if from God Himself, without doubting it, but believing firmly that his sins are forgiven by God in Heaven through it.

### Which sins should people confess?

When speaking to God, we should plead guilty to all sins, even those we don't know about, just as we do in the [Our Father](https://www.lutherananswers.com/boc/small-catechism/our-father/), but when speaking to the confessor, only the sins we know about, which we know about and feel in our hearts.

### Which are these?

Consider here your place in life according to the [Ten Commandments](https://www.lutherananswers.com/boc/small-catechism/10-commandments/).  Are you a father?  A mother? A son? A daughter? A husband? A wife? A servant? Are you disobedient, unfaithful or lazy? Have you hurt anyone with your words or actions? Have you stolen, neglected your duty, let things go or injured someone?