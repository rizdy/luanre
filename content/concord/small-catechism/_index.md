---
title: "The Small Catechism"
---

# The Small Catechism

### Martin Luther's Small Catechism as Translated by Robert E. Smith, July 11, 1994.

* [10 Commandments](/concord/small-catechism/10-commandments)
* [The Creed](/concord/small-catechism/creed)
* [Our Father](/concord/small-catechism/our-father)
* [Holy Baptism](/concord/small-catechism/holy-baptism)
* [Confession](/concord/small-catechism/confession)
* [Holy Communion](/concord/small-catechism/communion)
* [Daily Prayers](/concord/small-catechism/daily-prayers)