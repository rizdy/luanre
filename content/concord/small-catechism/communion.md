---
title: "The Sacrament of the Altar"
feature: stand-in.png
---

### What is the Sacrament of the Altar?

It is the true body and blood of our Lord Jesus Christ under bread and wine for us Christians to eat and to drink, established by Christ Himself.

### Where is that written?

The holy apostles Matthew, Mark and Luke and St. Paul write this:

> Our Lord Jesus Christ, in the night on which He was betrayed, took bread, gave thanks, broke it, gave it to His  disciples and said: "Take! Eat! This is My body, which is given for you. Do this to remember Me!" In the same way He also took the cup after supper, gave thanks, gave it to them, and said: "Take and drink from it, all of you! This cup is the New Testament in my blood, which is shed for you to forgive sins. This do, as often as you drink it, to remember Me!"

### What good does this eating and drinking do?

These words tell us: *Given for you* and *Shed for you to forgive sins.* Namely, that the forgiveness of sins, life and salvation are given to us through these words in the sacrament. Because, where sins are forgiven, there is life and salvation as well.

### How can physical eating and drinking do such great things?

Of course, eating and drinking do not do these things. These words, written here, do them: *given for you* and *shed for you to forgive sins.* These words, along with physical eating and drinking are the important part of the sacrament. Anyone who believes these words has what they say and what they record, namely, the forgiveness of sins.

### Who, then, receives such a sacrament in a worthy way?

Of course, fasting and other physical preparations are excellent disciplines for the body. But anyone who believes these words, "*Given for you*", and "*Shed for you to forgive sins*," is really worthy and well prepared.  But whoever doubts or does not believe these words is not worthy and is unprepared, because the words, *for you* demand a heart that fully believes.