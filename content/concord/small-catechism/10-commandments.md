---
title: "The Ten Commandments"
feature: ten-commandments.png
---

## The First Commandment

You must not have other gods.

**What does this mean?**

We must fear, love, and trust God more than anything else.

## The Second Commandment

You must not misuse your God's name.

**What does this mean?**

We must fear and love God, so that we will not use His name
to curse, swear, cast a spell, lie or deceive, but will use it to
call upon Him, pray to Him, praise Him and thank Him in all times
of trouble.

## The Third Commandment

You must keep the Sabbath holy.

**What does this mean?**

We must fear and love God, so that we will not look down on
preaching or God's Word, but consider it holy, listen to it
willingly, and learn it.

## The Fourth Commandment

You must honor your father and mother. [So that things will go
well for you and you will live long on earth].

**What does this mean?**

We must fear and love God, so that we will neither look down
on our parents or superiors nor irritate them, but will honor
them, serve them, obey them, love them and value them.

## The Fifth Commandment

You must not kill.

**What does this mean?**

We must fear and love God, so that we will neither harm nor
hurt our neighbor's body, but help him and care for him when he is
ill.

## The Sixth Commandment

You must not commit adultery.

**What does this mean?**

A. We must fear and love God, so that our words and actions
will be clean and decent and so that everyone will love and honor
their spouses.

## The Seventh Commandment

You must not steal.

**What does this mean?**

A. We must fear and love God, so that we will neither take our
neighbor's money or property, nor acquire it by fraud or by
selling him poorly made products, but will help him improve and
protect his property and career.

## The Eighth Commandment

You must not tell lies about your neighbor.

**What does this mean?**

A. We must fear and love God, so that we will not deceive by
lying, betraying, slandering or ruining our neighbor's reputation,
but will defend him, say good things about him, and see the best
side of everything he does.

## The Ninth Commandment

You must not desire your neighbor's house.

**What does this mean?**

A. We must fear and love God, so that we will not attempt to
trick our neighbor out of his inheritance or house, take it by
pretending to have a right to it, etc. but help him to keep &
improve it.

## The Tenth Commandment

You must not desire your neighbor's wife, servant, maid, animals
or anything that belongs to him.

**What does this mean?**

A. We must fear and love God, so that we will not release his
cattle, take his employees from him or seduce his wife, but urge
them to stay and do what they ought to do.

## The Conclusion to the Commandments

**What does God say to us about all these commandments?**

This is what He says:

I am the Lord Your God. I am a jealous God. I plague the
grandchildren and great-grandchildren of those who hate me with
their ancestor's sin. But I make whole those who love me for a
thousand generations.

**What does it mean?**

God threatens to punish everyone who breaks these commandments.
We should be afraid of His anger because of this and not violate
such commandments. But He promises grace and all good things to
those who keep such commandments. Because of this, we, too, should
love Him, trust Him, and willingly do what His commandments
require.