---
title: "The Sacrament of Holy Baptism"
feature: stand-in.png
---

### What is Baptism?

Baptism is not just plain water, but it is water contained
within God's command and united with God's Word.

#### Which Word of God is this?

The one which our Lord Christ spoke in the last chapter of
Matthew:
>> Go into all the world, teaching all heathen nations, and baptizing them in the name of the Father, the Son and of the Holy Spirit.

### What does Baptism give? What good is it?

It gives the forgiveness of sins, redeems from death and the
Devil, gives eternal salvation to all who believe this, just
as God's words and promises declare.

#### What are these words and promises of God?

Our Lord Christ spoke one of them in the last chapter of Mark:
>> Whoever believes and is baptized will be saved; but whoever does not believe will be damned.

### How can water do such great things?

Water doesn't make these things happen, of course. It is God's
Word, which is with and in the water. Because, without God's
Word, the water is plain water and not baptism. But with God's
Word it is a Baptism, a grace-filled water of life, a bath of
new birth in the Holy Spirit, as St. Paul said to Titus in the
third chapter:
>> Through this bath of rebirth and renewal of the Holy Spirit, which He poured out on us abundantly through Jesus Christ, our Savior, that we, justified by the same grace are made heirs according to the hope of eternal life. This is a faithful saying.

### What is the meaning of such a water Baptism?

It means that the old Adam in us should be drowned by daily
sorrow and repentance, and die with all sins and evil lusts,
and, in turn, a new person daily come forth and rise from
death again. He will live forever before God in righteousness
and purity.

### Where is this written?

St. Paul says to the Romans in chapter six:
>> We are buried with Christ through Baptism into death, so that, in the same way Christ is risen from the dead by the glory of the Father, thus also must we walk in a new life.