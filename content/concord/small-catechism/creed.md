---
title: "The Apostles Creed"
feature: stand-in.png
---

## The First Article: On Creation

*I believe in God the Almighty Father, Creator of Heaven and Earth.*

**What does this mean?**

I believe that God created me, along with all creatures. He gave to
me: body and soul, eyes, ears and all the other parts of my body,
my mind and all my senses and preserves them as well. He gives me
clothing and shoes, food and drink, house and land, wife and
children, fields, animals and all I own. Every day He abundantly
provides everything I need to nourish this body and life. He
protects me against all danger, shields and defends me from all
evil.  He does all this because of His pure, fatherly and divine
goodness and His mercy, not because I've earned it or desrved it.
For all of this, I must thank Him, praise Him, serve Him and obey
Him.  Yes, this is true!

## The Second Article: On Redemption

*And in Jesus Christ, His only Son, our Lord, Who was conceived by the
Holy Spirit, born of the Virgin Mary, suffered under Pontius Pilate,
was crucified, died and was buried, descended to Hell, on the third
day rose again from the dead, ascended to Heaven and sat down at the
right hand of God the Almighty Father. From there He will come to
judge the living and the dead.*

**What does this mean?**

I believe that Jesus Christ is truly God, born of the Father in
eternity and also truly man, born of the Virgin Mary. He is my
Lord! He redeemed me, a lost and condemned person, bought and won
me from all sins, death and the authority of the Devil. It did not
cost Him gold or silver, but His holy, precious blood, His innocent
body -- His death! Because of this, I am His very own, will live
under Him in His kingdom and serve Him righteously, innocently and
blessedly forever, just as He is risen from death, lives and reigns
forever. Yes, this is true.

## The Third Article: On Becoming Holy

*I believe in the Holy Spirit, the holy Christian Church, the community
of the saints, the forgiveness of sins, the resurrection of the body,
and an everlasting life. Amen.*

**What does this mean?**

I believe that I cannot come to my Lord Jesus Christ by my own
intellegence or power. But the Holy Spirit call me by the Gospel,
enlightened me with His gifts, made me holy and kept me in the true
faith, just as He calls, gathers together, enlightens and makes holy
the whole Church on earth and keeps it with Jesus in the one, true
faith. In this Church, He generously forgives each day every sin
committed by me and by every believer.  On the last day, He will raise
me and all the dead from the grave. He will give eternal life to me
and to all who believe in Christ. Yes, this is true!