---
title: "The Our Father"
feature: stand-in.png
---

## Introduction

*Our Father, Who is in Heaven.*

**What does this mean?**

In this introduction, God invites us to believe that He is our real
Father and we are His real children, so that we will pray with
trust and complete confidence, in the same way beloved children
approach their beloved Father with their requests.

## The First Request

*May Your name be holy.*

**What does this mean?**

Of course, God's name is holy in and of itself, but by this
request, we pray that He will make it holy among us, too.

**How does this take place?**

When God's Word is taught clearly and purely, and when we live holy
lives as God's children based upon it.  Help us, Heavenly Father,
to do this! But anyone who teaches and lives by something other
than God's Word defiles God's name among us. Protect us from this,
Heavenly Father!

## The Second Request

*Your Kingdom come.*

**What does this mean?**

Truly God's Kingdom comes by itself, without our prayer. But we
pray in this request that it come to us as well.

**How does this happen?**

When the Heavenly Father gives us His Holy Spirit, so that we
believe His holy Word by His grace and live godly lives here in
this age and there in eternal life.

## The Third Request

*May Your will be accomplished, as it is Heaven, so may it be on Earth.*

**What does this mean?**

Truly, God's good and gracious will is accomplished without our
prayer. But we pray in this request that is be accomplished among
us as well.

**How does this happen?**

When God destroys and interferes with every evil will and all evil
advice, which will not allow God's Kingdom to come, such as the
Devil's will, the world's will and will of our bodily desires. It
also happens when God strengthens us by faith and by His Word and
keeps living by them faithfully until the end of our lives.  This
is His will, good and full of grace.

## The Fourth Request

*Give us our daily bread today.*

**What does this mean?**

Truly, God gives daily bread to evil people, even without our
prayer. But we pray in this request that He will help us realize
this and receive our daily bread with thanksgiving.

**What does *Daily Bread* mean?**

Everything that nourishes our body and meets its needs, such as:
Food, drink, clothing, shoes, house, yard, fields, cattle, money,
possessions, a devout spouse, devout children, devout employees,
devout and faithful rulers, good government, good weather, peace,
health, discipline, honor, good friends, faithful neighbors and
other things like these.

## The Fifth Request

*And forgive our guilt, as we forgive those guilty of sinning against
us.*

**What does this mean?**

We pray in this request that our Heavenly Father will neither pay
attention to our sins nor refuse requests such as these because of our
sins and because we are neither worthy nor deserve the things for
which we pray. Yet He wants to give them all to us by His grace,
because many times each day we sin and truly deserve only punishment.
Because God does this, we will, of course, want to forgive from our
hearts and willingly do good to those who sin against us.

## The Sixth Request

*And lead us not into temptation.*

**What does this mean?**

God tempts no one, of course, but we pray in this request that God
will protect us and save us, so that the Devil, the world and our
bodily desires will neither deceive us nor seduce us into heresy,
despair or other serious shame or vice, and so that we will win and
be victorious in the end, even if they attack us.

## The Seventh Request

*But set us free from the Evil One.*

**What does this mean?**

We pray in this request, as a summary, that our Father in Heaven
will save us from every kind of evil that threatens body, soul,
property and honor. We pray that when at last our final hour has
come, He will grant us a blessed death, and, in His grace, bring us
to Himself from this valley of tears.

## Amen.

**What does this mean?**

That I should be certain that such prayers are acceptable to the
Father in Heaven and will be granted, that He Himself has commanded
us to pray in this way and that He promises to answer us. Amen.
Amen. This means: Yes, yes it will happen this way.