---
title: "Daily Prayers"
feature: stand-in.png
---

## Morning Devotions

As soon as you get out of bed in the morning, you should bless yourself with the sign of the Holy Cross and say:


May the will of God, the Father, the Son and the Holy Spirit be done! Amen.


Then, kneeling or standing, say the creed and pray the Lord's Prayer. If you wish, you may then pray this little prayer as well:


>> My Heavenly Father, I thank You, through Jesus Christ, Your beloved Son, that You kept me safe from all evil and danger last night. Save me, I pray, today as well, from every evil and sin, so that all I do and the way that I live will please you. I put myself in your care, body and soul and all that I have. Let Your holy Angels be with me, so that the evil enemy will not gain power over me. Amen.

After that, with joy go about your work and perhaps sing a song inspired by the Ten Commandments or your own thoughts.

## The Evening Devotions

When you go to bed in the evening, you should bless yourself with the sign of the Holy Cross and say:

May the will of God, the Father, the Son and the Holy Spirit be done! Amen.

Then, kneeling or standing, say the creed and pray the Lord's Prayer. If you wish, then you may pray this little prayer as well:


>> My Heavenly Father, I thank You, through Jesus Christ, Your beloved Son, that You have protected me, by Your grace. Forgive, I pray, all my sins and the evil I have done. Protect me, by Your grace, tonight. I put myself in your care, body and soul and all that I have. Let Your holy angels be with me, so that the evil enemy will not gain power over me. Amen.

After this, go to sleep immediately with joy.

## How to say Grace and Return Thanks at Meals:

The children and servants should come to the table modestly and with folded hands and say:

>> All eyes look to you, O Lord, and You give everyone food at the right time. You open Your generous hands and satisfy the hunger of all living things with what they desire.

*Note:*

"What they desire" means that all animals get so much to eat, that they are happy and cheerful. Because, worry and greed interferes with such desires.

After this, pray the Lord's Prayer and the following prayer:

>> Lord God, Heavenly Father, bless us and these gifts, which we receive from Your generous hand, through Jesus Christ, our Lord. Amen.