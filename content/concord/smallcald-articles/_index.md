---
title: "The Smallcald Articles"
---

Articles of Christian Doctrine which were to have been presented on our part to the Council, if any had been assembled at Mantua or elsewhere, indicating what we could accept or yield, and what we could not.

{{< section >}}