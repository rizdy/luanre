---
title: "Rev Fisk Interview"
date: 2021-07-19T06:00:00-04:00
feature: christ-is-king.jpg
length: 65   # In Minutes, round up.
description: I was blessed enough to be able to sit down for an hour with Reverend Jonathan Fisk and interview him about self-improvement and Christianity.
author: remy
audio:
categories: ["Interview"]
tags: ["Mad Christian", "Rev Fisk", "Lutheran"]
---

<div id="buzzsprout-player-8883140"></div>
<script src="https://www.buzzsprout.com/1772200/8883140-self-improvement-with-rev-jonathan-fisk.js?container_id=buzzsprout-player-8883140&player=small" type="text/javascript" charset="utf-8"></script>

This was my very first interview on my old Podcast Forged in Babylon. I was blessed to reach out to Reverend Jonathan Fisk and actually hear back from him! We set up a time and date and I interviewed him about self-improvement in Christianity. We talk about what it's like to be Christian in this world, being the best we can be, and what the Bible says about true Wisdom.  It was a wonderful time and I truly enjoyed getting to interact with Rev. Fisk. I hope you enjoy this episode as much as I enjoyed recording it!