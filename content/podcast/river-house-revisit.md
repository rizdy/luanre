---
title: "River House Revisit"
date: 2021-05-27T06:33:13-04:00
feature: river-house-two.png
length: 41   # In Minutes, round up.
description: We head back to *The River House* to check in on Pastor Long.
author: remy
audio: 8503620-buzzers-river-house-revisited
categories: ["Buzzers"]
tags: ["Buzzers", "Women's Ordination", "Biblical Pastor"]
---

<div id="buzzsprout-player-8503620"></div>
<script src="https://www.buzzsprout.com/1772200/8503620-buzzers-river-house-revisited.js?container_id=buzzsprout-player-8503620&player=small" async type="text/javascript" charset="utf-8"></script>

Welcome back to everyone's favorite game show: Buzzers! Where we play obnoxious sound effects whenever a pastor, preacher, or teacher says something blatantly heretical. Today we revisit Pastor Stacey Long at The River House Church in Fayetteville, NC, and his sermon on why women should wear head coverings. Wait, no, I'm sorry. His sermon is actually on why those passages of the Bible don't count, can be safely ignored, and why women can be pastors.