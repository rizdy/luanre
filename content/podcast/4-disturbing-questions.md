---
title: "4 Disturbing Questions"
date: 2021-07-05T06:33:50-04:00
feature: literally-chad.jpg
length: 55   # In Minutes, round up.
description: Ride along with @Literally_Chad and myself as we explore Tim Sledge's silly little booklet.
author: remy
audio: 8807644-4-questions-no-christian-can-answer-w-literally_chad
categories: ["Apologetics"]
tags: ["Ecumenical Council", "Catholicism", "Apologetics", "Atheists", "Neck Beards", "Tim Sledge"]
---

<div id="buzzsprout-player-8807644"></div>
<script src="https://www.buzzsprout.com/1772200/8807644-4-questions-no-christian-can-answer-w-literally_chad.js?container_id=buzzsprout-player-8807644&player=small" type="text/javascript" async charset="utf-8"></script>

In this fresh tasty episode I spent an hour speaking with Roman Catholic [@Literally_Chad](https://www.twitter.com/literally_chad) about Tim Sledge's ([@Goodbye_Jesus](https://www.twitter.com/goodbye_jesus)) book "[Four Disturbing Questions with One Simple Answer](https://amzn.to/3xmCemf)" - a book that supposedly no Christian can get through without doubting their faith.

Unsurprisingly we both made it through. 

I hope you enjoy this episode!