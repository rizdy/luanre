---
title: "Buzzers River House"
date: 2021-05-25T20:17:43-04:00
feature: river-house.jpg
length: 70 #In Minutes, round up.
description: In this Episode, Remy plays a game of buzzers with a sermon from local Fayetteville church "The River House"
author: remy
audio: 8502042-buzzers-river-house.mp3
categories: ["Sermon Review"]
tags: ["Charismatic", "sermons"]
---

<div id="buzzsprout-player-8502042"></div>
<script src="https://www.buzzsprout.com/1772200/8502042-buzzers-river-house.js?container_id=buzzsprout-player-8502042&player=small" type="text/javascript" async charset="utf-8"></script>

I'm reuploading another archive episode of Buzzers! This time I go through a sermon from a local church called The River House! It's awful. If you don't know how the game works, we listen to a sermon or prophecy or otherwise supposedly Christian piece of audio and stop it every so often to interject jokes, quips, or obnoxious sound effects. These things denote that what you've just heard is actually heresy, and not Christianity.