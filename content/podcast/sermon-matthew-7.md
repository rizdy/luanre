---
title: "Sermon Matthew 7"
date: 2021-07-26T06:00:00-04:00
feature: pulpit.jpg
length: 10 # In Minutes, round up.
description: Pastor Joe was on vacation and I was asked to give the message in his place.
author: remy
audio:
categories: ["Sermon"]
tags: ["Matthew 7", "Sermon", "Lutheran"]
---

<div id="buzzsprout-player-8916674"></div>
<script src="https://www.buzzsprout.com/1772200/8916674-bonus-sermon-on-matthew-7.js?container_id=buzzsprout-player-8916674&player=small" type="text/javascript" charset="utf-8"></script>

### Matthew 7:15-23
**A Tree and Its Fruit**
“Beware of false prophets, who come to you in sheep's clothing but inwardly are ravenous wolves. You will recognize them by their fruits. Are grapes gathered from thornbushes, or figs from thistles? So, every healthy tree bears good fruit, but the diseased tree bears bad fruit. A healthy tree cannot bear bad fruit, nor can a diseased tree bear good fruit. Every tree that does not bear good fruit is cut down and thrown into the fire. Thus you will recognize them by their fruits.

**I Never Knew You**
“Not everyone who says to me, ‘Lord, Lord,’ will enter the kingdom of heaven, but the one who does the will of my Father who is in heaven. On that day many will say to me, ‘Lord, Lord, did we not prophesy in your name, and cast out demons in your name, and do many mighty works in your name?’ And then will I declare to them, ‘I never knew you; depart from me, you workers of lawlessness.’

### Jeremiah 23:16-29
Thus says the Lord of hosts: “Do not listen to the words of the prophets who prophesy to you, filling you with vain hopes. They speak visions of their own minds, not from the mouth of the Lord. They say continually to those who despise the word of the Lord, ‘It shall be well with you’; and to everyone who stubbornly follows his own heart, they say, ‘No disaster shall come upon you.’”


For who among them has stood in the council of the Lord
    to see and to hear his word,
    or who has paid attention to his word and listened?

Behold, the storm of the Lord!
    Wrath has gone forth,
a whirling tempest;
    it will burst upon the head of the wicked.

The anger of the Lord will not turn back
    until he has executed and accomplished
    the intents of his heart.
In the latter days you will understand it clearly.


“I did not send the prophets,
    yet they ran;
I did not speak to them,
    yet they prophesied.

But if they had stood in my council,
    then they would have proclaimed my words to my people,
and they would have turned them from their evil way,
    and from the evil of their deeds.

“Am I a God at hand, declares the Lord, and not a God far away? Can a man hide himself in secret places so that I cannot see him? declares the Lord. Do I not fill heaven and earth? declares the Lord. I have heard what the prophets have said who prophesy lies in my name, saying, ‘I have dreamed, I have dreamed!’ How long shall there be lies in the heart of the prophets who prophesy lies, and who prophesy the deceit of their own heart, who think to make my people forget my name by their dreams that they tell one another, even as their fathers forgot my name for Baal? Let the prophet who has a dream tell the dream, but let him who has my word speak my word faithfully. What has straw in common with wheat? declares the Lord. Is not my word like fire, declares the Lord, and like a hammer that breaks the rock in pieces?

### Acts 20:27-38
for I did not shrink from declaring to you the whole counsel of God. Pay careful attention to yourselves and to all the flock, in which the Holy Spirit has made you overseers, to care for the church of God, which he obtained with his own blood. I know that after my departure fierce wolves will come in among you, not sparing the flock; and from among your own selves will arise men speaking twisted things, to draw away the disciples after them. Therefore be alert, remembering that for three years I did not cease night or day to admonish every one with tears. And now I commend you to God and to the word of his grace, which is able to build you up and to give you the inheritance among all those who are sanctified. I coveted no one's silver or gold or apparel. You yourselves know that these hands ministered to my necessities and to those who were with me. In all things I have shown you that by working hard in this way we must help the weak and remember the words of the Lord Jesus, how he himself said, ‘It is more blessed to give than to receive.’”

And when he had said these things, he knelt down and prayed with them all. And there was much weeping on the part of all; they embraced Paul and kissed him, being sorrowful most of all because of the word he had spoken, that they would not see his face again. And they accompanied him to the ship.