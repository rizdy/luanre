---
title: "Vicarage Don Stein"
date: 2021-08-16T06:00:00-04:00
feature: baby-baptism.jpg
length: 78 # In Minutes, round up.
description: I got to sit down with My Friend Don and discuss what his vicarage was like (among other things).
author: remy
audio:
categories: ["Interview"]
tags: ["My Friend Don", "Vicarage", "Lutheranism", "Ministry"]
---

<div id="buzzsprout-player-9009702"></div>
<script src="https://www.buzzsprout.com/1772200/9009702-vicarage-with-my-friend-don.js?container_id=buzzsprout-player-9009702&player=small" type="text/javascript" charset="utf-8"></script>

Here are the things mentioned in this episode:

[On the Resurrection of the Dead and On the Last Judgment - Theological Commonplaces (cph.org)](https://www.cph.org/p-34000-on-the-resurrection-of-the-dead-and-on-the-last-judgment-theological-commonplaces.aspx)

["The Myth of Righteous Anger" What the Bible Says About Human Anger by Concordia Seminary - issuu](https://issuu.com/concordiasem/docs/cp-themythofrighteousanger)

[Luther: Letters of Spiritual Counsel - Kindle edition by Luther, Martin, Tappert, Theodore G.. Religion & Spirituality Kindle eBooks @ Amazon.com.](https://amzn.to/3B3EvnN)