---
title: "Buzzers: Kay Nash"
date: 2021-05-26T06:33:39-04:00
feature: kay-nash-one.jpg
length: 28   # In Minutes, round up.
description: Laugh along as we play a game of buzzers with 'prophetess' Kay Nash.
author: remy
audio: 8441256-will-my-pets-be-in-heaven
categories: ["Buzzers"]
tags: ["Prophecy", "Buzzers", "Kay Nash", "False Prophet"]
---

<div id="buzzsprout-player-8441256"></div>
<script src="https://www.buzzsprout.com/1772200/8441256-will-my-pets-be-in-heaven.js?container_id=buzzsprout-player-8441256&player=small" async type="text/javascript" charset="utf-8"></script>

I've dug into my old audio archives and to bring back the show Buzzers! In this (re-aired) episode I take on a "prophecy" from the YouTube channel of Kay Nash, making sure to hit her with that sweet, sweet buzzer sound every time she says something incorrect. It's a lot of fun and I thought you guys would enjoy it!