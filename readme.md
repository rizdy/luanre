# Lutheran Answers 
### Built with Tailwind (and maybe AlpineJS, I'm not sure yet)

[![Netlify Status](https://api.netlify.com/api/v1/badges/4b112cca-88a0-411e-adb5-5443dfce5593/deploy-status)](https://app.netlify.com/sites/luanre/deploys)

Going along really well. I'm going to make a list here of things.

## TODO

* ~~Get Search Working~~
* ~~Answers Template~~
* ~~Answers Subsection Templates~~
* ~~Blog list template~~
* ~~Blog Post Template~~
* ~~Archetypes~~
* ~~Podcast List Template~~
* ~~Podcast Post Template~~
* ~~Form styling~~
* ~~Fancy content home page.~~
* ~~BoC List Template~~
* ~~BoC Section Template~~
* ~~BoC Entry Template~~
* Shop Page
* Product Page
* Thank You Pages
* Cancellation Pages
* More BoC Content.
* Make sure everything looks okay on mobile